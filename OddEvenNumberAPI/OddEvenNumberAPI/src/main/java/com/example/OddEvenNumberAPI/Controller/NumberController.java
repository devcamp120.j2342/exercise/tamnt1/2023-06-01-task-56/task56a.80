﻿package com.example.StringRestApi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.StringRestApi.Service.NumberService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class StringLengthController {
    @Autowired
    private NumberService service;

    @GetMapping("/checknumber")

    public int getLength(@RequestParam int number) {
        return service.checkOddNumber(number);
    }
}
