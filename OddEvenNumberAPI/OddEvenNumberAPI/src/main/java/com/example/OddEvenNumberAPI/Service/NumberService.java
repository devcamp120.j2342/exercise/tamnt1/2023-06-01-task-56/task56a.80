﻿package com.example.StringRestApi.Service;

import org.springframework.stereotype.Service;

@Service
public class StringLengthService {

    public int checkOddNumber(String number) {
        return number % 2 == 0 ? "This is odd number": "This is even number"
    }
}
